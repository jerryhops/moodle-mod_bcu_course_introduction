<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    mod
 * @subpackage bcucourseintro
 * @copyright  2014 Birmingham City University <michael.grant@bcu.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @global object
 * @param object bcucourseintro
 * @return bool|int
 */

function bcucourseintro_get_editor_options($context) {
  global $CFG;
  return array('subdirs'=>1, 'maxbytes'=>$CFG->maxbytes, 'maxfiles'=>-1, 'changeformat'=>1, 'context'=>$context, 'noclean'=>1, 'trusttext'=>0);
}

function bcucourseintro_add_instance($bcucourseintro, $mform = null) {
    global $DB;

    $cmid = $bcucourseintro->coursemodule;
    $context = context_module::instance($cmid);

    $editoroptions = bcucourseintro_get_editor_options($context);

    $bcucourseintro->timemodified = time();
    $bcucourseintro->name = 'bcucourseintro';
    $id = $DB->insert_record("bcucourseintro", $bcucourseintro);
    $DB->set_field('course_modules', 'instance', $id, array('id'=>$cmid));


    foreach($bcucourseintro->content as $key=>$val) {
        $record = new stdClass();
        $record->content = $bcucourseintro->content[$key]['text'];
        $record->introid = $id;
        $record->name = $bcucourseintro->sectioname[$key];
        $record->templateid = $bcucourseintro->templateid[$key];
        $record->timemodified = time();
        $record->id = $DB->insert_record('bcucourseintro_sections', $record);
        if ($mform and !empty($bcucourseintro->content[$key]['itemid'])) {
          $record->content = file_save_draft_area_files($bcucourseintro->content[$key]['itemid'], $context->id, 'mod_bcucourseintro', "content", $record->id, bcucourseintro_get_editor_options($context), $bcucourseintro->content[$key]['text']);
          $DB->update_record('bcucourseintro_sections', $record);
        }
    }

    return $id;
}

/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @global object
 * @param object bcucourseintro
 * @return bool
 */
function bcucourseintro_update_instance($bcucourseintro) {
    global $DB;

    $context = context_module::instance($bcucourseintro->coursemodule);
    $editoroptions = array(
        'noclean' => true,
        'maxfiles' => EDITOR_UNLIMITED_FILES,
        'trusttext'=>true
    );

    $bcucourseintro->timemodified = time();
    $bcucourseintro->id = $bcucourseintro->instance;
    foreach($bcucourseintro->content as $key=>$val) {
        $record = new stdClass();
        $record->content = $bcucourseintro->content[$key]['text'];
        $record->timemodified = time();
        if(ISSET($bcucourseintro->templateid[$key])) {
          $record->introid = $bcucourseintro->instance;
          $record->templateid = $bcucourseintro->templateid[$key];
          $record->name = $bcucourseintro->sectioname[$key];
          $record->id = $DB->insert_record('bcucourseintro_sections', $record);
          $record->content = file_save_draft_area_files($bcucourseintro->content[$key]['itemid'], $context->id, 'mod_bcucourseintro', "content", $record->id, bcucourseintro_get_editor_options($context), $bcucourseintro->content[$key]['text']);
        } else {
          $record->id = $key;
          $record->name = $bcucourseintro->sectioname[$key];
          $record->content = file_save_draft_area_files($bcucourseintro->content[$key]['itemid'], $context->id, 'mod_bcucourseintro', "content", $record->id, bcucourseintro_get_editor_options($context), $bcucourseintro->content[$key]['text']);
          $DB->update_record('bcucourseintro_sections', $record);
        }
    }
    return $DB->update_record("bcucourseintro", $bcucourseintro);
}

function bcucourseintro_update_template($bcucourseintro) {
    global $DB;
    foreach($bcucourseintro->content as $key=>$val) {
        $section = new stdClass();
        $section->introid = 0;
        $section->templateid = 0;
        $section->name = $bcucourseintro->sectioname[$key];
        $section->content = $val['text'];
        $section->timemodified = time();
        if (isset($bcucourseintro->sectionid[$key]) && !empty($bcucourseintro->sectionid[$key])){//existing record record
            $section->id=$bcucourseintro->sectionid[$key];
            if (isset($val['text']) && trim($val['text']) <> '') {
                $DB->update_record("bcucourseintro_sections", $section);
            } else {
                $DB->delete_records("bcucourseintro_sections", array("id"=>$section->id));
            }
        } else {
            $DB->insert_record("bcucourseintro_sections", $section);
        }
    }

}

/**
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @global object
 * @param int $id
 * @return bool
 */
function bcucourseintro_delete_instance($id) {
    global $DB;
    try {
        $transaction = $DB->start_delegated_transaction();

        $bcucourseintro = $DB->get_record("bcucourseintro", array("id" => $id));
        $DB->delete_records("bcucourseintro", array("id" => $bcucourseintro->id));
        $DB->delete_records("bcucourseintro_sections", array("introid" => $bcucourseintro->id));
         $transaction->allow_commit();
        return true;
    } catch (Exception $ex) {
        $transaction->rollback($e);
        return false;
    }
}

function bcucourseintro_get_sections($id) {
    global $DB;
    if (! $bcucourseintro = $DB->get_records("bcucourseintro_sections", array("introid" => $id))) {
        return false;
    }

    return $bcucourseintro;
}

function bcucourseintro_check_sections($template, $current) {
    // Check whether the sections still exist in the template, if not mark for delete.
    foreach($current as $section) {
        if($section->templateid != $template[$section->templateid]->id) {
            $current[$section->id]->delete = true;
        }

        if($section->templateid == $template[$section->templateid]->id) {
            $template[$section->templateid]->found = true;
        }
    }

    foreach($template as $temp) {
        if(!ISSET($temp->found)) {
            $template[$temp->id]->found = false;
        }
    }
    return array('current'=>$current, 'template'=>$template);
}

function bcucourseintro_supports($feature) {
    switch($feature) {
        case FEATURE_IDNUMBER:                return false;
        case FEATURE_GROUPS:                  return false;
        case FEATURE_GROUPINGS:               return false;
        case FEATURE_GROUPMEMBERSONLY:        return true;
        case FEATURE_MOD_INTRO:               return false;
        case FEATURE_COMPLETION_TRACKS_VIEWS: return false;
        case FEATURE_GRADE_HAS_GRADE:         return false;
        case FEATURE_GRADE_OUTCOMES:          return false;
        case FEATURE_MOD_ARCHETYPE:           return MOD_ARCHETYPE_RESOURCE;
        case FEATURE_BACKUP_MOODLE2:          return true;
        case FEATURE_NO_VIEW_LINK:            return true;

        default: return null;
    }
}

function bcucourseintro_get_coursemodule_info($coursemodule) {
    global $DB, $COURSE;
    $warning = html_writer::tag('div', 'Please update this section with '.html_writer::tag('strong', 'relevant information'), array('class' => 'alert alert-danger'));
    $issues = false;
    if ($bcucourseintro = $DB->get_record('bcucourseintro', array('id' => $coursemodule->instance))) {
        if($sections = bcucourseintro_get_sections($coursemodule->instance)) {
            $defaultsections = bcucourseintro_get_defaultsections();
            $content = '';
            foreach($sections as $section) {
                $context = context_module::instance($coursemodule->id);
                $section->content = file_rewrite_pluginfile_urls($section->content,
                                                                    'pluginfile.php',
                                                                    $context->id,
                                                                    'mod_bcucourseintro',
                                                                    'content',
                                                                    $section->id);
                if($defaultsections[$section->templateid]->content == $section->content) {
                    $issues = true;
                    $content .= bcucourseintro_toggle_outline($section->name.$section->id, $section->name, $section->content);
                    $content .= $warning;
                } else {
                    $content .= bcucourseintro_toggle_outline($section->name.$section->id, $section->name, $section->content);
                }
            }
        } else {
            return false;
        }

        $info = new cached_cm_info();
        $info->content = $content;
        $info->customdata = array('issues'=>$issues);
        return $info;
    } else {
        return false;
    }
}

function bcucourseintro_cm_info_dynamic(cm_info $cm) {
    global $COURSE;
    if (($cm->customdata['issues']) && !has_capability('moodle/course:update', context_course::instance($COURSE->id))) {
        $cm->set_user_visible(false);
    } else {
        $cm->set_user_visible(true);
    }
}

function bcucourseintro_toggle_outline($togglename, $toggletitle, $togglecontent) {
    $output = html_writer::start_tag('div', array(
        'class' => 'mod-bcucourseintro-toggle'
    ));

    $output .= html_writer::start_tag('div', array(
        'class' => 'panel-heading'
    ));

    $output .= html_writer::start_tag('h4', array(
        'class' => 'panel-title'
    ));

    $output .= html_writer::tag('a', $toggletitle, array(
        'data-toggle' => 'collapse',
        'class' => 'intro-toggle collapsed',
        'href' => '#'.str_replace(' ','',$togglename)
    ));

    $output .= html_writer::end_tag('h4');

    $output .= html_writer::end_tag('div');

    $output .= html_writer::start_tag('div', array(
        'id' => str_replace(' ','',$togglename),
        'class' => 'panel-collapse collapse'
    ));

    $output .= html_writer::tag('div', $togglecontent, array(
        'class' => 'panel-body'
    ));

    $output .= html_writer::end_tag('div');

    $output .= html_writer::end_tag('div');

    return $output;
}

function bcucourseintro_get_defaultsections() {
    return bcucourseintro_get_sections(0);
}

function bcucourseintro_pluginfile($course, $cm, $context, $filearea='content', $args, $forcedownload, array $options=array()) {
    global $CFG, $DB;
    require_login($course, false, $cm);

    require_once($CFG->libdir.'/filelib.php');
    $relativepath = implode('/', $args);
    $fullpath = "/{$context->id}/mod_bcucourseintro/$filearea/$relativepath";
    $fs = get_file_storage();
    if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
        return false;
    }

     // finally send the file
    send_stored_file($file, 0, 0, true, $options); // download MUST be forced - security!

    return false;
}
