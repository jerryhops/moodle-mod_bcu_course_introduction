<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package mod_label
 * @copyright  2010 onwards Eloy Lafuente (stronk7) {@link http://stronk7.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the backup steps that will be used by the backup_label_activity_task
 */

/**
 * Define the complete label structure for backup, with file and id annotations
 */
class backup_bcucourseintro_activity_structure_step extends backup_activity_structure_step {

    protected function define_structure() {

        // To know if we are including userinfo
        $userinfo = $this->get_setting_value('userinfo');

        $bcucourseintro = new backup_nested_element('bcucourseintro', array('id'), array('name', 'timemodified'));
        
        $sections = new backup_nested_element('bcucourseintro_sections');
        
        $section = new backup_nested_element('bcucourseintro_section', array('id'), array('introid', 'name', 'content', 'timemodified', 'templateid'));
        
         // Build the tree
        
        $sections->add_child($section);
        $bcucourseintro->add_child($sections);
        
        // Define sources
        $bcucourseintro->set_source_table('bcucourseintro', array('id' => backup::VAR_ACTIVITYID));
        $section->set_source_table('bcucourseintro_sections', array('introid' => backup::VAR_PARENTID));

        return $this->prepare_activity_structure($bcucourseintro);
    }
}
