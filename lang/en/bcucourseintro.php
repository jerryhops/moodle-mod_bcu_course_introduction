<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'resource', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package    mod_resource
 * @copyright  1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['modulename'] = 'Course Introduction';
$string['modulename_help'] = 'Course introductions allows you to display an introduction to each course based upon a centrally defined template.';
$string['modulename_link'] = 'mod/bcu_course_introduction/view';
$string['modulenameplural'] = 'Course Introductions';
$string['page-mod-resource-x'] = 'Any file module page';
$string['pluginadministration'] = 'Course introduction administration';
$string['pluginname'] = 'Course introduction';

$string['sectiontitle'] = 'Section title';
$string['sectiontext'] = 'Section content';
$string['sections'] = 'Sections';