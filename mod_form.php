<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    mod_bcucourseintro
 * @copyright  2014 Birmingham City University <michael.grant@bcu.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/course/moodleform_mod.php');

class mod_bcucourseintro_mod_form extends moodleform_mod {
  function definition() {
    GLOBAL $CFG, $COURSE;
    $context = \context_course::instance($COURSE->id);
    $editoroptions = bcucourseintro_get_editor_options($context);

    $mform = $this->_form;

    $mform->addelement('header', 'sectionhdr', get_string('sections', 'mod_bcucourseintro'));

    $sections = bcucourseintro_get_sections($this->_instance);
    $defaultsections = bcucourseintro_get_defaultsections();
    if($sections  && $this->_instance) {
      $checked = bcucourseintro_check_sections($defaultsections, $sections);
      foreach($sections as $section) {
          $mform->addElement('hidden', 'sectioname['.$section->id.']', $defaultsections[$section->templateid]->name);
          $mform->addElement('html', html_writer::tag('h3', $section->name));
          if($defaultsections[$section->templateid]->name !== $section->name) {
              $mform->addElement('html', html_writer::tag('div', 'This template for this section has been updated. '.html_writer::tag('strong', $section->name).' is now '.html_writer::tag('strong', $defaultsections[$section->templateid]->name), array('class' => 'alert alert-danger')));
          }
          if(isset($checked['current'][$section->id]->delete) && $checked['current'][$section->id]->delete) {
              $mform->addElement('html', html_writer::tag('div', 'This section has been deleted from the template', array('class' => 'alert alert-danger')));
          }
          $mform->addElement('editor', 'content['.$section->id.']', get_string('sectiontext', 'mod_bcucourseintro'), null, $editoroptions);

          $mform->setType('content['.$section->id.']', PARAM_RAW);
          $mform->setType('sectioname['.$section->id.']', PARAM_RAW);
      }

      foreach($checked['template'] as $template) {
        if($template->found) {
          continue;
        }
        $mform->addElement('html', html_writer::tag('div', 'This section has been added to the template', array('class' => 'alert alert-danger')));
        $mform->addElement('html', html_writer::tag('h3', $template->name));
        $mform->addElement('hidden', 'sectioname['.$template->id.']', $template->name);
        $mform->addElement('hidden', 'templateid['.$template->id.']', $template->id);
        $mform->addElement('editor', 'content['.$template->id.']', get_string('sectiontext', 'mod_bcucourseintro'), null, $editoroptions);


        $mform->setType('sectioname['.$template->id.']', PARAM_RAW);
        $mform->setType('templateid['.$template->id.']', PARAM_RAW);
        $mform->setType('content['.$template->id.']', PARAM_RAW);
      }
    } else {
      // No sections exist, so prompt the user to create new ones
      if(!$defaultsections) {
          $mform->addElement('html', html_writer::tag('p', "You haven't configured the sections in the admin section yet"));
      } else {
        foreach($defaultsections as $section) {
          $mform->addElement('html', html_writer::tag('h3', $section->name));
          $mform->addElement('hidden', 'sectioname['.$section->id.']', $section->name);
          $mform->addElement('hidden', 'templateid['.$section->id.']', $section->id);
          $mform->addElement('editor', 'content['.$section->id.']', get_string('sectiontext', 'mod_bcucourseintro'), null, $editoroptions);

          $mform->setType('sectioname['.$section->id.']', PARAM_RAW);
          $mform->setType('templateid['.$section->id.']', PARAM_RAW);
          $mform->setType('content['.$section->id.']', PARAM_RAW);
        }
      }
    }

    $this->standard_coursemodule_elements();
    $this->add_action_buttons(true, false, null);
  }

  function data_preprocessing(&$default_values) {
    global $COURSE;
    $context = \context_course::instance($COURSE->id);
    $editoroptions = bcucourseintro_get_editor_options($context);
    $sections = bcucourseintro_get_sections($this->_instance);
    foreach($sections as $section) {
      $draftitemid = file_get_submitted_draft_itemid('content['.$section->id.']');
      var_dump($draftitemid);
      $default_values['content['.$section->id.']']['text'] =
          file_prepare_draft_area($draftitemid, $this->context->id,
          'mod_bcucourseintro', 'content', $section->id,
          $editoroptions,
          $section->content);

      $default_values['content['.$section->id.']']['format'] = 1;
      $default_values['content['.$section->id.']']['itemid'] = $draftitemid;
    }
  }
}
