<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    mod_bcucourseintro
 * @copyright  2014 Birmingham City University <michael.grant@bcu.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

class mod_bcucourseintro_sections_form extends moodleform {

    function definition() {
        GLOBAL $CFG, $DB;
        
        $mform = $this->_form;

        $mform->addelement('header', 'sectionhdr', get_string('sections', 'mod_bcucourseintro'));
        $mform->addelement('hidden', 'instance', 0);
        $mform->setType('instance', PARAM_RAW);
        $sections = bcucourseintro_get_defaultsections();
        
            /*
            foreach($sections as $section) {
                $mform->addElement('text', 'sectioname['.$section->id.']', get_string('sectiontitle', 'mod_bcucourseintro'));
                $mform->addElement('editor', 'content['.$section->id.']', get_string('sectiontext', 'mod_bcucourseintro'));

                $mform->setDefault('sectioname['.$section->id.']', $section->name);
                $mform->setDefault('content['.$section->id.']', array('text'=>$section->content));

                $mform->setType('sectioname['.$section->id.']', PARAM_RAW);
                $mform->setType('content['.$section->id.']', PARAM_RAW);
                
                $mform->addRule('sectioname['.$section->id.']', get_string('error'), 'required', null, null, false, false);
            }
            */
        
        $repeatarray = array();
        $repeatarray[] = $mform->createElement('text', 'sectioname', get_string('sectiontitle', 'mod_bcucourseintro'));
        $repeatarray[] = $mform->createElement('editor', 'content', get_string('sectiontext', 'mod_bcucourseintro'));
        $repeatarray[] = $mform->createElement('hidden', 'sectionid', 0);
        
        $repeatno = $DB->count_records('bcucourseintro_sections', array('introid' => 0));
        if($repeatno == 0) {
            $repeatno = 1;
        }
        
        $repeateloptions = array();
        $repeateloptions['sectioname']['default'] = 0;
        $repeateloptions['content']['default'] = array('text' => 0 );
        $repeateloptions['limit']['type'] = PARAM_INT;
        
        $mform->setType('sectionid', PARAM_INT);
        $mform->setType('content', PARAM_CLEANHTML);
        $mform->setType('sectioname', PARAM_CLEANHTML);
        
        $this->repeat_elements($repeatarray, $repeatno,
                    $repeateloptions, 'option_repeats', 'option_add_fields', 1, null, true);
        
        $this->add_action_buttons(true, 'Save');

    }
    
    function set_data($default_values) {
        $this->data_preprocessing($default_values);
        parent::set_data($default_values);
    }
    
    function data_preprocessing(&$default_values){
        global $DB;
        $options = $DB->get_records_menu('bcucourseintro_sections',array('introid'=>0), 'id', 'id,name');
        $options2 = $DB->get_records_menu('bcucourseintro_sections', array('introid'=>0), 'id', 'id,content');
        $choiceids=array_keys($options);
        $options=array_values($options);
        $options2=array_values($options2);
        foreach (array_keys($options) as $key){
            $default_values['sectioname['.$key.']'] = $options[$key];
            $default_values['content['.$key.']'] = array('text' => $options2[$key]);
            $default_values['sectionid['.$key.']'] = $choiceids[$key];
        }

        
        if (empty($default_values['timeopen'])) {
            $default_values['timerestrict'] = 0;
        } else {
            $default_values['timerestrict'] = 1;
        }
    }
}
