<?php
require_once('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->dirroot.'/mod/bcucourseintro/lib.php');
require_once($CFG->dirroot.'/mod/bcucourseintro/sections_form.php');

admin_externalpage_setup('mod_bcucourseintro');

$mform = new mod_bcucourseintro_sections_form();

$PAGE->set_pagelayout('admin');
$PAGE->set_title('Course introductions');
$PAGE->set_heading('Course introductions');
echo $OUTPUT->header();

//Form processing and displaying is done here
if ($mform->is_cancelled()) {
    //Handle form cancel operation, if cancel button is present on form
} else if ($fromform = $mform->get_data()) {
    bcucourseintro_update_template($fromform);
    $mform->set_data();
    $mform->display();
  //In this case you process validated data. $mform->get_data() returns data posted in form.
} else {
  // this branch is executed if the form is submitted but the data doesn't validate and the form should be redisplayed
  // or on the first display of the form.
    $mform->set_data();
  //displays the form
  $mform->display();
}


echo $OUTPUT->footer();